# URLShortener API #

![URLs Shortener API](images/main-ui.png)

 
**Overview of the URLShortener API** :

This Repository contains the implementation of the the URLShortener API.
The URLShortener API is a JAVA developed microservices application that exposes endpoints to generate short URLs.
	
- Git Repo for Codes 
	**- https://rusamson2013@bitbucket.org/rusamson2013/urlshortener.git**
	
- Docker Hub for image 
	**- https://hub.docker.com/r/rusamson2013/urlshortener**
	
## HOW TO RUN ##

pull and run using the following Docker command
 : $ sudo docker run --rm -p 8080:8080 rusamson2013/urlshortener


when the application will fully run :

1 -Go to the main interface on the swagger page
	 ( http://localhost:8080/swagger-ui.html ) and select **mainController** : 

 	https://bitbucket.org/rusamson2013/urlshortener/src/master/images/main-ui.png


2 - Select "POST" and fill in all fields
 
	**longURL** for the long URL (eg. https://www.caranddriver.com/features/g15383134/most-beautiful-cars/ )
	**size** for the alias size (eg. 3 )
	**hostname** for the hostname of the computer to do the forwad (eg. http://localhost:8080/app/ )


Then press on **Try It Out**	(it should be looking like below )

![URLs Shortener API](images/post-input.png)

and you should see the Response as below :

![URLs Shortener API](images/post-response.png)



3 - Take the new created short URL and paste it in a browser and you should be forwaded to the long URL 
    
	the short URL will look similar to this **(http://localhost:8080/app/HNs")**



4 - For **STATISTICS** - do to **'GET'** and press **Try it Out** : 
     You should get the JSON reporting that the URL was clicked once as below image 

![URLs Shortener API](images/after-click-1.png)


5 - try to use the short URL many times and the clicks counts should increament as below :

![URLs Shortener API](images/after-click-2.png)

5 -**SPRING BOOT MONITORING** using Actuator and HAL 

# URLShortener API #

![URLs Shortener API](images/monitor.png)



### Dependencies
- Java 8
- Docker 
	

### Tools 
 
- Spring Boot 
	- For the main application 
	
- JPA - Hibernate 
	- for perisistance
	
- H2 Database 
	- for in memory or File persisted Database 
	
- Maven 
    - as a build automation tool

- Swagger 
	- Swagger - for the UI 
	

### How to Pull and run the APP from Docker Hub ###

- The Remote Docker image can be found here 
	**https://hub.docker.com/r/rusamson2013/urlshortener**

- To pull and  Run the application from Docker Hub :
	**- $ sudo docker run --rm -p 8080:8080 rusamson2013/urlshortener**


### How run the APP from Eclipse ###

- Import the project as Maven project into Eclipse
	-Right click on **SpringBootWebApplication** and Run as Java Application

### H2 Database configuration ###

By Default ,I had to set the database to be in memory to avoid the write access issues that may rise. 
If there is a need of having it saved on the local machine , you can change the **application.properties** file located in the Resources folder as below 

	from  :   **spring.datasource.url=jdbc:h2:mem:urlsdb;DB_CLOSE_ON_EXIT=FALSE**
	To    :   **spring.datasource.url=jdbc:h2:file:/data/urlsdb**

