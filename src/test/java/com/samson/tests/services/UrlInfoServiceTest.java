package com.samson.tests.services;

import static org.assertj.core.api.Assertions.assertThat;
 

import org.junit.Test; 
import org.junit.runner.RunWith; 
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired; 
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager; 
import org.springframework.context.annotation.ComponentScan; 
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
 
import com.samson.model.UrlInfo; 
import com.samson.service.UrlInfoService;  

 
@RunWith(SpringJUnit4ClassRunner.class) 
@DataJpaTest 
@ComponentScan("com.samson.*")
public class UrlInfoServiceTest {
 
	private  Logger logger = LoggerFactory.getLogger(UrlInfoServiceTest.class);
	
	@Autowired(required=true)
	private TestEntityManager entityManager;
 
	@Autowired(required=true)
	private UrlInfoService urlInfoService;   

	@Test
	public void testIfgenerateUrlOkay(){ 
		UrlInfo urlInfo =  getUrlInfo();
		UrlInfo savedUrlInfo = urlInfoService.generateUrl(urlInfo);
		assertThat(urlInfo).isEqualTo(savedUrlInfo);  
		assertThat(urlInfo.getShortUrl()).isEqualTo(savedUrlInfo.getShortUrl()); 		
	} 
	
	@Test
	public void testIfgenerateAliasSizeOkay(){ 
		UrlInfo urlInfo =  getUrlInfo();
		UrlInfo savedUrlInfo = urlInfoService.generateUrl(urlInfo);
		assertThat(urlInfo.getSize()).isEqualTo(savedUrlInfo.getPath().length() );   	
	} 
	
	  
	@Test
	public void testFindByPathOkay(){ 
		UrlInfo urlInfo =  getUrlInfo();
		entityManager.persist(urlInfo);
		UrlInfo savedUrlInfo = urlInfoService.getUrlInfoByPath("dgHG4");
		assertThat(urlInfo.getSize()).isEqualTo(savedUrlInfo.getPath().length() );   	
	}  
		
	private UrlInfo getUrlInfo() {
		UrlInfo urlInfo = new UrlInfo();
		urlInfo.setClicks(0);
		urlInfo.setHostname("http://localhost:8080/"); 
		urlInfo.setLongURL("https://www.carbuyer.co.uk/reviews/recommended");
		urlInfo.setPath("dgHG4");
		urlInfo.setSize(5);  
		return urlInfo;
	}
}
