package com.samson.service;
 
import java.util.List;

import javax.transaction.Transactional;

import org.apache.commons.lang3.RandomStringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
 
import com.samson.model.UrlInfo;
import com.samson.repository.UrlInfoDao; 
/**
* <h1>UrlInfo Service Class</h1>
* This class is for the servicing capabilities 
* It takes  care of all the Business Logic
* 
* @author  Samson Rukundo
* @version 1.0
* @since   14-08-2020
*/

@Service 
@Transactional
public class UrlInfoServiceImpl implements UrlInfoService {
   
    private  Logger logger = LoggerFactory.getLogger(UrlInfoServiceImpl.class);

	@Autowired
	private UrlInfoDao urlInfoDao;  
	

	   /**
	   * This method is used to generate the URL prefix known as the Path. 
	   * first of all ; it generates random characters based on the size requested
	   * after it add those characters to the UrlObject before saving the object to the DB. 
	   * @param urlInfo, This is the only parameter to be used
	   * @return UrlInfo, This is the freshly saved URLInfo.
	   */
	@Override
	public UrlInfo generateUrl(UrlInfo urlInfo) {  
		String generatedString = RandomStringUtils.randomAlphanumeric(urlInfo.getSize());   
		urlInfo.setPath(generatedString);  
		urlInfo.setShortUrl(urlInfo.getHostname()+generatedString);
		int urlInfoId = urlInfoDao.insert(urlInfo); 
		logger.info("generated prefix is {}  on  {} Hostname ", generatedString , urlInfo.getHostname());
		return urlInfoDao.find(urlInfoId);
	}

	   /**
	   * This method is used to return the URLInfo based on the path. 
	   * It also increment the clicks by one every time accessed 
	   * @param path, This is the only parameter to be used
	   * @return UrlInfo, This is the freshly updated URLInfo.
	   */
	@Override
	public UrlInfo getUrlInfoByPath(String path) {
		UrlInfo urlInfo = urlInfoDao.findByPath(path);
		int clicks = urlInfo.getClicks();
		urlInfo.setClicks(clicks+1); 
		urlInfoDao.update(urlInfo);
		return urlInfo;
	} 

	   /**
	   * This method is used to return all Urls Information from the DAO.  
	   * @return List, This is the list of all urlInfos.
	   */
	@Override
	public List<UrlInfo>  getAllUrlInfo(){  
		return urlInfoDao.findAll();
	}
}
