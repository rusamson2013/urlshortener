package com.samson.service;

import java.util.List;

import com.samson.model.UrlInfo;

/**
* <h1>UrlInfo Service Interface</h1>
* This gives a list of methods that will need to be implemented
* 
* @author  Samson Rukundo
* @version 1.0
* @since   14-08-2020
*/

public interface UrlInfoService {

	/**
	* This method is used to return the URLInfo based on the path.  
	* @param path, This is the only parameter to be used
	* @return UrlInfo, This is the freshly updated URLInfo.
	*/
public UrlInfo getUrlInfoByPath(String path);

/**
* This method is used to generate the URL prefix known as the Path.  
* @param urlInfo, This is the only parameter to be used
* @return UrlInfo, This is the freshly saved URLInfo.
*/
public UrlInfo generateUrl(UrlInfo urlInfo);

/**
* This method is used to return all Urls Information.  
* @return List, This is the list of all urlInfos.
*/
public List<UrlInfo> getAllUrlInfo();

}
