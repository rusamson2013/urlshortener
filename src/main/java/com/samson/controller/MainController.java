package com.samson.controller;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;
 
import com.samson.exception.NoURLMatchingException;
import com.samson.model.UrlInfo;
import com.samson.model.UrlInfoDto;
import com.samson.service.UrlInfoService;
import com.samson.service.UrlInfoServiceImpl;  

/**
* <h1>Main Controller</h1>
* This is the main Controller
* it gives the entry point to the system
* 
* @author  Samson Rukundo
* @version 1.0
* @since   14-08-2020
*/

@Controller
@RequestMapping("app")
public class MainController {
 
	private  Logger logger = LoggerFactory.getLogger(MainController.class);

	@Bean
	public ModelMapper modelMapper() {
	    return new ModelMapper();
	}
	
	@Bean
	public UrlInfo urlInfo() {
	    return new UrlInfo();
	}
	
	@Autowired
	private UrlInfoService urlInfoService;   

    @Autowired
    private ModelMapper modelMapper;
 
    @Autowired
    private UrlInfo urlInfo;
	
	   /**
	   * This method is used to access the main page (index).  
	   * @param model, This is the model
	   * @return String, This is the name of the main page.
	   */
	@RequestMapping("/")
	public String mainPage(Map<String, Object> model) {  
	    return "index";
	}

	   /**
	   * This method is used to create the shortUrl and return it. .  
	   * @param urlsCollector, This the UrlInfo object
	   * @param model, This is the model object
	   * @return UrlInfo, Return the freshly created entity .
	   */
	
    @RequestMapping(value = "/createurl", method = RequestMethod.POST , produces = "application/json")
	public ResponseEntity<UrlInfo>  createShortURL(@Valid @ModelAttribute("UrlInfoDto") UrlInfoDto urlInfoDto) {  
    	  
    	modelMapper.map(urlInfoDto, urlInfo); 
    	
    	String urlexpression = "^(https?|ftp|file)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]";

    	String longUrl = urlInfo.getLongURL().trim();
    	int size = urlInfo.getSize();
    	String hostname = urlInfo.getHostname().trim(); 
    	
    	try {   		 
	    	if(longUrl.matches(urlexpression) && hostname.matches(urlexpression) && size > 0 ) {
	    		
	    		return new ResponseEntity<UrlInfo>(urlInfoService.generateUrl(urlInfo), HttpStatus.OK);
	    	  } 
    	}catch(Exception ex) { 
    		logger.error("couldn't create because Request not well formatted !", ex); 
    	}
		return new ResponseEntity<UrlInfo>(new UrlInfo(hostname,longUrl, size ), HttpStatus.BAD_REQUEST);
 	}
    
	   /**
	   * This method returns all the URLinfo created.  
	   * @return List, Return the List of all UlrInfo .
	   */  
    @RequestMapping(value = "/all", method = RequestMethod.GET , produces = "application/json")
	public ResponseEntity<List> getAllUrlInfo() {
    	List<UrlInfo> allData =  urlInfoService.getAllUrlInfo();
	return new ResponseEntity<List>(allData, HttpStatus.OK);
	} 
 
	   /**
	   * This method is used to Redirect to the longURL.  
	   * @param prefix, This is the alias 
	   * @param urlInfo, This the UrlInfo object
	   * @return RedirectView, Returns the RedirectView to redirect to .
	   */
    @GetMapping("/{prefix}")
    public RedirectView redirectToTHeLongURL(@PathVariable("prefix") String prefix, UrlInfo urlInfo) { 
	     try {
	    	 urlInfo = urlInfoService.getUrlInfoByPath(prefix);  
	     }catch( Exception ex) {
	    	    logger.error("No matching URL could be foud !");
	    	 throw new NoURLMatchingException("No matching URL could be foud !");
	     }
	    logger.info("requested prefix is {}  redirecting to  {} URL ", prefix , urlInfo.getLongURL());
	    return new RedirectView(urlInfo.getLongURL()); 
    }  
 
}