package com.samson.exception;

public class NoURLMatchingException extends RuntimeException{

	/**
	 * 
	 */
	
	private static final long serialVersionUID = 1L;
	
	private String message;
	public NoURLMatchingException(String message) {
		super();
		this.message = message;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	
}
