package com.samson.exception;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.AnnotationUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
 
/**
* <h1>this is the GlobolDefaultException Handler</h1>
*
* 
* @author  Samson Rukundo
* @version 1.0
* @since   14-08-2020
*/

@ControllerAdvice
public class GlobalDefaultExceptionHandler { 

	   /**
	   * This method will redirect the user to the error page in a 
	   * case of errors where no longUrl found in the System.   
	   * @param NoURLMatchingException, This is the NoURLMatchingException
	   * @return ModelAndView, the ModelAndView to return.
	   */
	
	@ExceptionHandler({NoURLMatchingException.class})
	public ModelAndView handleException(NoURLMatchingException ex) 
	{ 
		ModelAndView modelAndView = new ModelAndView();
	    modelAndView.setViewName("errorpage");
	    modelAndView.addObject("message", ex.getMessage());
	    return modelAndView;
	}
 
}
