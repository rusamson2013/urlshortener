package com.samson.model;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity 
@NamedQueries({
    @NamedQuery(name="urlInfofindAll",
                query="SELECT u FROM UrlInfo u"),
    @NamedQuery(name="urlInfofindByPath",
                query="SELECT u FROM UrlInfo u WHERE u.path = :path"),
}) 
@Table(name = "UrlInfo")
public class UrlInfo {

@Id 
@GeneratedValue(strategy = GenerationType.AUTO)
private int id;
private String path;
private String longURL;
private int clicks;
private int size;
private String hostname;
private String shortUrl;

public UrlInfo() {}

 

public int getId() {
	return id;
}



public void setId(int id) {
	this.id = id;
}



public String getPath() {
	return path;
}



public void setPath(String path) {
	this.path = path;
}



public String getLongURL() {
	return longURL;
}



public void setLongURL(String longURL) {
	this.longURL = longURL;
}



public int getClicks() {
	return clicks;
}



public void setClicks(int clicks) {
	this.clicks = clicks;
}



public int getSize() {
	return size;
}



public void setSize(int size) {
	this.size = size;
}



public String getHostname() {
	return hostname;
}



public void setHostname(String hostname) {
	this.hostname = hostname;
}



public String getShortUrl() {
	return shortUrl;
}



public void setShortUrl(String shortUrl) {
	this.shortUrl = shortUrl;
}



public UrlInfo(String hostname, String longURL, int size) {
	super();
	this.longURL = longURL;
	this.size = size;
	this.hostname = hostname;
}



@Override
public String toString() {
	return "UrlInfo [id=" + id + ", path=" + path + ", longURL=" + longURL + ", clicks=" + clicks + ", size=" + size
			+ ", hostname=" + hostname + ", shortUrl=" + shortUrl + "]";
}


 
 

 
}
