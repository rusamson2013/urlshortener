package com.samson.model;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

public class UrlInfoDto {  
	@NotNull(message="longURL cannot be null")
	private String longURL; 
	@NotNull(message="size  cannot be null")
	@Min(value=1, message="The size should be greater than 0")
	private int size;
	@NotNull(message="hostname should not be null")
	private String hostname;
	
	public String getLongURL() {
		return longURL;
	}
	public void setLongURL(String longURL) {
		this.longURL = longURL;
	}
	public int getSize() {
		return size;
	}
	public void setSize(int size) {
		this.size = size;
	}
	public String getHostname() {
		return hostname;
	}
	public void setHostname(String hostname) {
		this.hostname = hostname;
	}
  
}
