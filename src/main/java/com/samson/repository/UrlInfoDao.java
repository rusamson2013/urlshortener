package com.samson.repository;

import java.util.List;

import com.samson.model.UrlInfo;

/**
* <h1>UrlInfo Data Access Interface</h1>
* This gives a list of methods that will need to be implemented
* 
* @author  Samson Rukundo
* @version 1.0
* @since   14-08-2020
*/

public interface UrlInfoDao {
	
	   /**
	   * This method is used insert the UrlInfo to the DB.  
	   * @param urlInfo, This is the only parameter to be used
	   * @return int, This is the ID of the inserted UrlInfo.
	   */
	public int insert(UrlInfo urlInfo);

	   /**
	   * This method is used to return the URLInfo form the DB.  
	   * @param id, This is the ID of the UrlInfo that needs to be found
	   * @return UrlInfo, This is the found UrlInfo.
	   */
	public UrlInfo find(int id);

	   /**
	   * This method that update the URLInfo .  
	   * @param urlInfo, The urlInfo should be passed as the parameter 
	   */
	public void update(UrlInfo urlInfo);

	   /**
	   * This method is used to delete the URLInfo from the DB.  
	   * @param urlInfo, you pass in the urlInfo that needs to be deleted 
	   */
	public void delete(UrlInfo urlInfo);

	   /**
	   * This method is used to return the URLInfo based on the path from the DB.  
	   * it uses a namedQuery defined in the Entity Class
	   * @param path, This is the only parameter to be used
	   * @return UrlInfo, This is the found URLInfo.
	   */
	public UrlInfo findByPath(String path);

	   /**
	   * This method is used to return all URLInfo existing in the DB.   
	   * @return UrlInfo, This is the List of all URLInfo from the DB.
	   */
	public List<UrlInfo> findAll();
}
