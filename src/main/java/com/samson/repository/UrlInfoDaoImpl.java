package com.samson.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.samson.model.UrlInfo; 

/**
* <h1>UrlInfo Data Access Class</h1>
* This deals with the persistence 
* It takes  care of all the DB CRUIDs
* 
* @author  Samson Rukundo
* @version 1.0
* @since   14-08-2020
*/

@Repository
@Transactional
public class UrlInfoDaoImpl  implements UrlInfoDao{
	
	private  Logger logger = LoggerFactory.getLogger(UrlInfoDaoImpl.class);
	
	@PersistenceContext
	private EntityManager entityManager;

	   /**
	   * This method is used insert the UrlInfo to the DB.  
	   * @param urlInfo, This is the only parameter to be used
	   * @return int, This is the ID of the inserted UrlInfo.
	   */
	@Override	
	public int insert(UrlInfo urlInfo) {
	    logger.info("persisting UrlInfo to DB");
		entityManager.persist(urlInfo);
		return urlInfo.getId();
	}

	   /**
	   * This method is used to return the URLInfo form the DB.  
	   * @param id, This is the ID of the UrlInfo that needs to be found
	   * @return UrlInfo, This is the found UrlInfo.
	   */
	@Override
	public UrlInfo find(int id) {
		logger.info("finding the UrlInfo by id");
		return entityManager.find(UrlInfo.class, id);
	}

	   /**
	   * This method that update the URLInfo .  
	   * @param urlInfo, The urlInfo should be passed as the parameter 
	   */
	@Override
	public void update(UrlInfo urlInfo) {
		logger.info("Updating the UrlInfo");
		entityManager.merge(urlInfo); 
 	}

	   /**
	   * This method is used to delete the URLInfo from the DB.  
	   * @param urlInfo, you pass in the urlInfo that needs to be deleted 
	   */
	@Override
	public void delete(UrlInfo urlInfo) {
		logger.info("deleting the UrlInfo");
		entityManager.remove(urlInfo); 
	}

	   /**
	   * This method is used to return the URLInfo based on the path from the DB.  
	   * it uses a namedQuery defined in the Entity Class
	   * @param path, This is the only parameter to be used
	   * @return UrlInfo, This is the found URLInfo.
	   */
	@Override	
	public UrlInfo findByPath(String path) { 
		   logger.info("finding the data by path");
			Query query = entityManager.createNamedQuery("urlInfofindByPath", UrlInfo.class);
			query.setParameter("path", path); 
		return (UrlInfo) query.getResultList().get(0);
	}

	   /**
	   * This method is used to return all URLInfo existing in the DB.   
	   * @return UrlInfo, This is the List of all URLInfo from the DB.
	   */
	@Override	
	public List<UrlInfo> findAll() { 
		    logger.info("finding all UrlInfo from the DB");
			Query query = entityManager.createNamedQuery("urlInfofindAll", UrlInfo.class);
		return query.getResultList();
	}


}