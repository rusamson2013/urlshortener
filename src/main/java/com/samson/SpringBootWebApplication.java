package com.samson; 

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;

/**
* <h1>URL Shortener project</h1>
*This API generate a shot URL for any given long Url 
* @author  Samson Rukundo
* @version 1.0
* @since   2014-08-14
*/

@SpringBootApplication
public class SpringBootWebApplication extends SpringBootServletInitializer {
	  
    private static Logger logger = LoggerFactory.getLogger(SpringBootWebApplication.class);

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(SpringBootWebApplication.class);
	}

	public static void main(String[] args) throws Exception {
		SpringApplication.run(SpringBootWebApplication.class, args);
		logger.info("Application started");
	}

}