<!DOCTYPE html>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html lang="en">
<head>

  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>

 
<c:url value="/css/main.css" var="jstlCss" />
<link href="${jstlCss}" rel="stylesheet" />

</head>
<body>

	<nav class="navbar navbar-inverse">
		<div class="container">
			<div class="navbar-header">
				<a class="navbar-brand" href="#">URLS Shortener</a>
			</div>
			<div id="navbar" class="collapse navbar-collapse">
				<!--<ul class="nav navbar-nav">
					<li class="active"><a href="#">Home</a></li>
					<li><a href="#about">Statistics</a></li>
				</ul>-->
			</div>
		</div>
	</nav> 
	<!-- /.container -->

<div class="container-fluid"> 


	<label for="longurl">Enter short URL Size:</label>
    <input type="text" name="size" id="size" class="form-control width100">
	<label for="longurl">Enter a Hostname:</label>
    <input type="text" name="hostname" id="hostname" value="http://localhost:8080/app/" class="form-control width100">    
 	<label for="longurl">Enter a long URL:</label>
 	<div class="input-group">
    <input type="text" name="longURL" id="longURL" class="form-control width100">
    <span class="input-group-btn">
        <button type="submit" class="btn btn-info">Create Short URL</button>
    </span>
   </div> 

 <div> Generated URL is <span id="shortURL" ></span>  </div>
</div>

<div class="container-fluid">
 <table class="table">
  <thead class="thead-dark">
    <tr>
      <th scope="col">clicks</th>    
      <th scope="col">Hostname</th>    	
      <th scope="col">Path</th>
      <th scope="col">Path Size</th>
      <th scope="col">shortUrl</th>
      <th scope="col">longUrl</th>

    </tr>
  </thead>
  <tbody id="tbody">
  </tbody>
</table>
 
</div>
	<script type="text/javascript"
		src="webjars/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<script>


$(document).ready(function(){
  $("button").click(function(){ 

/*
$.ajax({
        url : "/createurl",
        type: "POST",
        data: JSON.stringify([
            { longURL: $("#longURL").val() , hostname: $("#hostname").val() , size: $("#size").val() }
        ]),
        contentType: "application/json; charset=utf-8",
        dataType   : "json",
        success    : function(data){ 
           	  var str = data; 
	          $("#shortURL").html(data['shortURL']);
	          drawTable();
        }
    });
*/
  $.post( "/app/createurl", { longURL: $("#longURL").val() , hostname: $("#hostname").val() , size: $("#size").val() })
   .done(function( data ) { 
	  var str = data; 
	  $("#shortURL").html(data['shortURL']);
	  drawTable();
     });
 
  });
  drawTable();
});

function drawTable(){
 $.get("/app/all", function(data, status){  
   var urls = "";  
  	 	for (var i = data.length - 1; i >= 0; i--) { 
  urls += '<tr><th scope="row">'+data[i]['clicks']+'</th><td>'+data[i]['hostname']+'</td><td>'+data[i]['path']+'</td><td>'+data[i]['size']+'</td><td> <a href="'+data[i]['hostname']+data[i]['path']+'">'+data[i]['hostname']+data[i]['path']+'</a></td> <td>'+data[i]['longURL']+' </td></tr>';
  	 }
    $("#tbody").html(urls); 
  });
}

function updateStats(){
   drawTable();
}

</script>


</body>

</html>
